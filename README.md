# Gbackground

![](https://gitlab.com/pondogor/gbackground/-/raw/master/icon/config-xfree.png) 

**How use?**

If you want to use gbackground you should run gbackground script.
For  this exist two ways:

`$ ./gbackground`

**Or:**

`$ perl gbackground`

This program was developed in Perl using Gtk2

http://www.mogaal.com/gbackground

http://gbackground.sourceforge.net/

https://sourceforge.net/projects/gbackground/

**Changelog**

2020-07-12

* Backup copy of the original project

2010-09-12 - Alejandro Garrido Mota <garridomota@gmail.com>
gbackground (1.3-1) unstable; urgency=low
* New upstream release (Closes: #595027, #595043)
* Remove DH_VERBOSE in d/rules
* Update Standards-Version field 
* Add libyaml-perl like Depends
* Adapt d/copyright to DEP5
* Add source format 3.0

2009-08-25 - Alejandro Garrido Mota <garridomota@gmail.com>
gbackground (1.2-1) unstable; urgency=low
* New upstream release (Closes: #533311).
* Add watch file.
* Remove libglib2.0-03 from Depends field, It was
a mistake to have included in Depends field.
* Remove version's package from Depends field.
* Update Standards-Version to 3.8.3 
* Remove space after '\ ' in menu file
* Improved short and large description
* Implemented dh7
* Add gbackground.install file
* Update to GPL-3+ package license

2009-04-11 - Alejandro Garrido Mota <garridomota@gmail.com>
gbackground (1.1-2) unstable; urgency=low
* Add libui-dialog-perl package to Depends field (Closes: #522346)

2009-01-08 - Alejandro Garrido Mota <garridomota@gmail.com>
gbackground (1.1-1) unstable; urgency=low
* New upstream release
* Improve:
- debian/rules file
- copyright file
- man page
- Changelog file
* Change Standards-Version to 3.8.0
* Support debhelper 7 compatibility
* Change menu section to: Screen/Locking
* Include english translation (Closes: #391490)

2006-09-10 - Alejandro Garrido Mota <garridomota@gmail.com>
gbackground (1.0-1) unstable; urgency=low
* Initial release (Closes: #387034)

